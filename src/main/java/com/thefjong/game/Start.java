package com.thefjong.game;

import javafx.application.Application;
import javafx.stage.Stage;

public class Start extends Application {

    /**
     * Vi har en parent class "Application" som skal have loaded en masse ting.
     * Derfor kalder vi en parent funktion der hedder "launch();"
     *
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }

    /**
     * Når parent klassen er færdige med sine ting, bliver denne funktion kaldt.
     * Deri fortæller vi at spille skal køre ved at kalde en statisk metode i Game classen.
     *
     * @param stage
     */
    @Override
    public void start(Stage stage) {
        Game.run(stage);
    }
}
