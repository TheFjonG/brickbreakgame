package com.thefjong.game;

import com.thefjong.game.components.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class BrickBreakGame {

    public static BrickBreakGame currentGame;
    public static int lastScore = 0;
    public ArrayList<IComponent> tickingComponents = new ArrayList<>();
    public ArrayList<BrickComponent> bricks = new ArrayList<>();
    public ArrayList<Map.Entry<Boolean, Node>> deferredChanges = new ArrayList<>();
    public Pane canvas;
    public Scene scene;
    public int score = 0;
    public int brickCount = 8;
    public Timeline timeline;

    /**
     * Her laver vi scenen til selve spillet og fortæller vinduet at den skal centrere sig midt på skærmen.
     */
    public void run() {
        canvas = new Pane();
        scene = new Scene(canvas, 800, 800, Color.TRANSPARENT);

        Game.stage.setScene(scene);
        Game.stage.centerOnScreen();
        currentGame = this;

        init();
    }

    /**
     * Her tilføjet vi en timeline hvilket i realiteten gør at vi får vores dejlige framerate.
     * <p>
     * Dertil kalder vi alle funktion til at tilføje elementerne til scenen.
     */
    public void init() {
        timeline = new Timeline(new KeyFrame(Duration.millis(1), t -> {
            Iterator<IComponent> iterator = tickingComponents.listIterator();
            while (iterator.hasNext()) {
                iterator.next().tick();
            }
        }));
        addBall(true);
        addSlate();
        addBricks(brickCount);
        addScore();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    /**
     * Tiløjet score component og ligger det i denne klasses children, så den også bliver rendered.
     * Ticking components er componenter der skal rerender hvert tick
     */
    public void addScore() {
        ScoreComponent component = new ScoreComponent(10, scene.getHeight() - 10);
        canvas.getChildren().add(component);
        tickingComponents.add(component);
    }

    /**
     * Tilføjer bolden
     *
     * @param isStart
     */
    public void addBall(boolean isStart) {
        BallComponent component = new BallComponent(10, Color.DARKSLATEBLUE, isStart);
        canvas.getChildren().add(component);
        tickingComponents.add(component);
    }

    /**
     * Tilføjer pladen vi styrer
     */
    public void addSlate() {
        SlateComponent component = new SlateComponent(80, 10, Color.DARKSLATEBLUE);
        canvas.getChildren().add(component);
        tickingComponents.add(component);
    }

    /**
     * Tilføjer alle bricks.
     *
     * @param count
     */
    public void addBricks(int count) {
        int times = 0;
        int rounds = 0;
        for (int i = 0; i < count; i++) {

            BrickComponent component = new BrickComponent(50, 20, Color.BROWN);
            // Dette betyder at for hvert 8. brick, laver vi en ny linje
            if (i != 0 && i % 8 == 0) {
                times = 0;
                rounds++;
            }
            times++;
            component.relocate(60 + (times * 50 + (times * 20)), 90 + (rounds * 20 + (rounds * 10)));
            canvas.getChildren().add(component);
            bricks.add(component);
        }
    }

    /**
     * Denne metode stopper spiller og sætter scenen til at være main menu
     */
    public void die() {
        timeline.stop();
        lastScore = score;
        Game.stage.setScene(Scenes.mainMenu());
    }
}
