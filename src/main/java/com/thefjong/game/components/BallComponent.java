package com.thefjong.game.components;

import com.thefjong.game.BrickBreakGame;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

import java.util.Iterator;


public class BallComponent extends Circle implements IComponent {

    public static final double INCREASE_SPEED = 0.1;
    public static final int INITIAL_WAIT_TIME = 3000;

    public static int ballAmount = 1;
    public double dx = 0.5;
    public double dy = 0.25;
    public boolean isStart;
    public int hits;
    public long init;
    public long lastHit;
    public boolean paused = true;

    public BallComponent(double radius, Paint fill, boolean isStart) {
        super(radius, fill);
        this.isStart = isStart;
        init = lastHit = System.currentTimeMillis();
        center();
    }

    /**
     * Centrere bolden in das middle
     */
    public void center() {
        relocate(BrickBreakGame.currentGame.canvas.getWidth() / 2, BrickBreakGame.currentGame.canvas.getHeight() / 2);
    }

    public void tick() {
        moveBall();

        checkCollisions();
    }

    /**
     * Styre når bolden rammer en væk eller et bestemt component
     */
    private void checkCollisions() {
        handleWindowCollision();

        Iterator<Node> iterator = BrickBreakGame.currentGame.canvas.getChildren().listIterator();

        while (iterator.hasNext()) {
            Node child = iterator.next();

            if (this.getBoundsInParent().intersects(child.getBoundsInParent())) {
                if (child instanceof SlateComponent) {
                    handleSlateCollision(child);
                }
                if (child instanceof BrickComponent) {
                    handleBrickCollision(iterator, child);
                }
            }
            // Håndtere hvis der er flere bolde i luften. Hvis bolden har ramt bricks flere end 5 gange, fjern bolden.
            if (child == this && hits >= 5 && !isStart) {
                ballAmount -= 1;
                iterator.remove();
            }
        }
    }

    /**
     * Håndtere når bolden rammer en Brick.
     *
     * @param iterator
     * @param child
     */
    private void handleBrickCollision(Iterator<Node> iterator, Node child) {
        // Tilføjet 1 point til score når du rammer en brick.
        BrickBreakGame.currentGame.score++;

        Bounds childBounds = child.getBoundsInParent();
        if (this.getLayoutX() <= (childBounds.getMinX() + this.getRadius()) || this.getLayoutX() >= (childBounds.getMaxX() - this.getRadius())) {
            dx = -dx;
        }

        if ((this.getLayoutY() >= (childBounds.getMaxY() - this.getRadius())) || (this.getLayoutY() <= (childBounds.getMinY() + this.getRadius()))) {
            dy = -dy;
        }
        hits++;

        // Hvis bricken er død, fjern bricken fra spillet, og
        if (((BrickComponent) child).damage(1)) {
            BrickBreakGame.currentGame.bricks.remove(child);

            // Hvis spillet har 0 bricks tilbage, tilføj 8+ sidste gang
            if (BrickBreakGame.currentGame.bricks.size() == 0) {
                paused = true;
                center();
                BrickBreakGame.currentGame.addBricks(BrickBreakGame.currentGame.brickCount += 8);
                // Og gør farten hurtigere
                increaseSpeed();
            }
            iterator.remove();

            paused = false;
        }
    }

    /**
     * Håndtere når bolden rammer en styrebare slate.
     *
     * @param child
     */
    private void handleSlateCollision(Node child) {
        if (lastHit < System.currentTimeMillis() - 500) {
            Bounds childBounds = child.getBoundsInParent();

            if (this.getLayoutX() <= (childBounds.getMinX() + this.getRadius()) || this.getLayoutX() >= (childBounds.getMaxX() - this.getRadius())) {
                dx = -dx;
            }

            if ((this.getLayoutY() >= (childBounds.getMaxY() - this.getRadius())) || (this.getLayoutY() <= (childBounds.getMinY() + this.getRadius()))) {
                dy = -dy;
            }
            lastHit = System.currentTimeMillis();
        }
        // Hvis vi ville implementere flere bolde, så udkommentere linjen nedenunder
        //((SlateComponent) child).hit();
    }

    /**
     * Flytter bolden og håndtere start pausen
     */
    public void moveBall() {
        // Dette er for at bolden ikke starter så snart spillet loades.
        if (System.currentTimeMillis() - INITIAL_WAIT_TIME > init) {
            paused = false;
        }
        // Hvis vi ikke er startet, så flyt bolden med antal speed.
        if (!paused) {
            this.setLayoutX(this.getLayoutX() + dx);
            this.setLayoutY(this.getLayoutY() + dy);
        }
    }

    /**
     * Håndtere vinduets kant.
     */
    protected void handleWindowCollision() {
        Bounds bounds = BrickBreakGame.currentGame.canvas.getLayoutBounds();

        // Hvis den rammer vinduets kant på x aksen, så gå den anden vej. Vi inverter egentlig bare variablet mellem minus og plus
        if (this.getLayoutX() <= (bounds.getMinX() + this.getRadius()) || this.getLayoutX() >= (bounds.getMaxX() - this.getRadius())) {
            dx = -dx;
        }

        // Hvis bolden rammer bunden, dræb spillet
        if ((this.getLayoutY() >= (bounds.getMaxY() - this.getRadius()))) {
            BrickBreakGame.currentGame.die();
        }
        // Hvis bolden rammer toppen, gå nedaf igen.
        if ((this.getLayoutY() <= (bounds.getMinY() + this.getRadius()))) {
            dy = -dy;
        }
    }


    /**
     * Øger hastigen på bolden med x
     */
    public void increaseSpeed() {
        dx = dx > 0 ? dx + INCREASE_SPEED : dx - INCREASE_SPEED;
        dy = dy > 0 ? dy + INCREASE_SPEED : dy - INCREASE_SPEED;
    }
}
