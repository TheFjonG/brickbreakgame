package com.thefjong.game.components;

import com.thefjong.game.BrickBreakGame;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class ScoreComponent extends Text implements IComponent {

    public static final int FONT_SIZE = 40;

    /**
     * Creates a new instance of Rectangle with the given size and fill.
     *
     * @param x
     * @param y
     */
    public ScoreComponent(double x, double y) {
        super(x, y, "");
        setFont(Font.font(FONT_SIZE));
    }

    /**
     * Hvert eneste tick opdatere vi dette components tekst.
     * HVERT ENESTE TICK - damnnit
     */
    public void tick() {
        setText("Score: " + BrickBreakGame.currentGame.score);
    }
}
