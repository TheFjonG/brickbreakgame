package com.thefjong.game.components;

import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

public class BrickComponent extends Rectangle implements IComponent {

    public int health = 1;

    /**
     * Creates a new instance of Rectangle with the given size and fill.
     *
     * @param width
     * @param height
     * @param fill
     */
    public BrickComponent(double width, double height, Paint fill) {
        super(width, height, fill);
    }

    /**
     * Sætter en damage på dette brick.
     * Dette er til en fremtidig feature, at bricks skal rammes flere gange, fx.
     *
     * @param count
     * @return hvis denne brick er død
     */
    public boolean damage(int count) {
        health -= count;
        return health == 0;
    }

    /**
     * Dette component bliver ikke lagt i TickingComponents i BrickBreakGame, så denne metode bliver aldrig kaldt.
     */
    @Override
    public void tick() {
        // Do nothing
    }
}
