package com.thefjong.game.components;

import com.thefjong.game.BrickBreakGame;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;


public class SlateComponent extends Rectangle implements IComponent {

    public static final int MAX_BALL_AMOUNT = 2;
    double mouseX = 0;
    int hits;

    /**
     * Creates a new instance of Rectangle with the given size and fill.
     *
     * @param width  width of the rectangle
     * @param height height of the rectangle
     * @param fill   determines how to fill the interior of the rectangle
     */
    public SlateComponent(double width, double height, Paint fill) {
        super(width, height, fill);

        // Relocater dette component til midten
        relocate(BrickBreakGame.currentGame.canvas.getWidth() / 2, BrickBreakGame.currentGame.canvas.getHeight() - BrickBreakGame.currentGame.canvas.getHeight() / 6);

        // Tilføjer en event listener til musen, som bliver kaldt når musen flytter sig. Herefter sætter vi Variablet MouseX
        BrickBreakGame.currentGame.scene.addEventFilter(MouseEvent.MOUSE_MOVED, event -> mouseX = event.getSceneX());
    }

    /**
     * Her relocater vi dette component efter MouseX variablet
     */
    @Override
    public void tick() {
        if (mouseX != 0) {
            relocate(mouseX - getWidth() / 2, getLayoutY());
            mouseX = 0;
        }
    }

    /**
     * Når dette component rammes laver vi en change for at tilføje en ekstra bold.
     * Denne metode bliver dog ikke kaldt da den ikke implementeret
     */
    public void hit() {
        hits++;
        if (hits > 6) {
            if (BallComponent.ballAmount <= MAX_BALL_AMOUNT) {
                BrickBreakGame.currentGame.addBall(false);
                BallComponent.ballAmount++;
            }
            hits = 0;
        }
    }
}
