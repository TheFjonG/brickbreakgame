package com.thefjong.game.components;

public interface IComponent {

    // Bare en Interface der siger vi SKAL have tick metoden
    public void tick();
}
