package com.thefjong.game;

import javafx.scene.Scene;
import javafx.stage.Stage;

public class Game {

    public static Stage stage;
    public static Scene DEFAULT_SCENE = Scenes.mainMenu();

    /**
     * Funktionen bliver kaldt når spillet skal kører.
     * Heri sætter vi titlen og sætter scencen vi får fra mainWindow() metoden i Scenes klassen.
     *
     * @param stage
     */
    public static void run(Stage stage) {
        Game.stage = stage;

        //stage.initStyle(StageStyle.TRANSPARENT);
        stage.setTitle("Ballers");
        stage.setScene(Game.DEFAULT_SCENE);

        stage.show();
    }
}
