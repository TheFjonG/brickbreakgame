package com.thefjong.game;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Scenes {


    /**
     * Her laver vi scenen hvorpå Play knappen er.
     *
     * @return
     */
    public static Scene mainMenu() {

        Pane canvas = new Pane();
        Scene scene = new Scene(canvas, 300, 300, Color.TRANSPARENT);

        // Hvis der var en last score, tilføj "Last Score" teksten til vinduet.
        if (BrickBreakGame.lastScore != 0) {
            Text text = new Text(10, 20, "Last Score: " + BrickBreakGame.lastScore);
            text.setFont(Font.font(20));
            canvas.getChildren().add(text);
        }

        // Tilføj play knappen og kør (new BrickBreakGame()).run() når der bliver klikket på den
        Button playButton = new Button("Play Game!");
        playButton.setPrefSize(100, 50);
        playButton.setLayoutX((scene.getWidth() / 2) - playButton.getPrefWidth() / 2);
        playButton.setLayoutY((scene.getHeight() / 2) - playButton.getPrefHeight() / 2);
        playButton.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            (new BrickBreakGame()).run();
        });


        canvas.getChildren().add(playButton);

        return scene;
    }

}
